<?php

namespace Drupal\easy_360\Hook;

/**
 * Holds methods implementing hooks related to theming.
 */
class Theming {

  /**
   * Implements hook_theme().
   *
   * @return array
   *   An associative array of information about theme implementations.
   */
  public function theme() {
    $themes = [];
    $themes['easy_360_viewer'] = [
      'variables' => [
        // Each item corresponds to an image in the viewer. It must be an
        // associative array with two elements:
        // - large_image: The URL to the large image that is displayed when
        //   zooming into the item.
        // - small_image: The URL to the small image that is displayed in the
        //   viewer.
        'items' => [],
      ],
      'template' => 'viewer',
    ];

    return $themes;
  }

}
