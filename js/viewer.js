/**
 * @file
 * Easy 360 Viewer behaviors.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches Easy 360 Viewer behavior to the image field.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the Easy 360 Viewer behavior to the image field.
   */
  Drupal.behaviors.easy360Viewer = {
    attach: function (context) {
      registerViewer();
    }
  };

  /**
   * Initiates the viewer with the default settings.
   *
   * @I Support multiple viewers per page with dynamic IDs
   *    type     : feature
   *    priority : high
   *    labels   : field
   *
   * @I Support passing custom configuration
   *    type     : bug
   *    priority : high
   *    labels   : config, field
   */
  function registerViewer() {
    // The documentation instructs calling `FWDUtils.onReady()`; that
    // responds to an document-ready event. However, we are inside a behavior
    // and the code runs when the document is ready so the event has already
    // happened when `FWDUtils` subscribes to it.
    // The only code really happening in `FWDUtils.onReady()` apart from
    // registering the viewer is this line that creates a `div`, so we do that
    // directly here to avoid having to call `FWDUtils.onReady()` that doesn't
    // work.
    document.documentElement.appendChild(FWDUtils.dumy);

    // Initialize the viewer.
    var viewer = new FWDViewer({
      // Main.
      divHolderId:"easy-360-viewer",
      skinPath:"/libraries/easy-360-product-viewer/start/skins/skin1",
      playListId:"easy-360-viewer-playlist",
      displayType:"responsive",
      initializeOnlyWhenVisible:"no",
      useVectorIcons:"no",
      startDraggingMode:"rotate",
      useEntireScreenFor3dObject:"no",
      stopRotationHorizontalAtEnds:"no",
      stopRotationVerticalAtEnds:"no",
      inverseRotation:"no",
      addDragAndSpinSupport:"yes",
      disableMouseWheelZoom:"no",
      autoScale:"yes",
      dragVertical:"no",
      startSlideShowOnLeave:"no",
      startAtImage:0,
      startAtSet:0,
      viewerWidth:1000,
      viewerHeight:658,
      viewerMinHeight:450,
      imageWidth:500,
      imageHeight:500,
      imageOffsetX:0,
      imageOffsetY:0,
      zoomFactor:2.2,
      zoomSpeed:.2,
      dragRotationSpeed:.7,
      dragAndSpinSpeed:.6,
      buttonsRotationSpeed:300,
      slideShowDelay:700,
      backgroundColor:"#FFFFFF",
      // Preloader.
      preloaderText:"Loading object: ",
      preloaderPosition:"center",
      preloaderOffsetX:40,
      preloaderOffsetY:40,
      showPreloaderProgressBar:"yes",
      preloaderWidth:"70%",
      preloaderProgressBarBackgroundColor:"#8b859b",
      preloaderProgressBarFillColor:"#bfbf06",
      preloaderWindowBackgroundOpacity:.7,
      preloaderFontColor:"#000000",
      preloaderBackgroundSize:"large",
      preloaderBackgroundColor:"#FFFFFF",
      // Lightbox.
      lightBoxWidth:1000,
      lightBoxHeight:700,
      lightBoxBackgroundOpacity:.8,
      lightBoxBackgroundColor:"#000000",
      // Controller.
      buttons:"rotate, pan, roteteleft,rotateright, scrollbar, play, info, link, fullscreen",
      buttonsToolTips:" Rotate, Move/Pan, Rotate left, Rotate right, Zoom level: , Play/Pause, Info, Custom link, Full screen/Normal screen",
      showControllerBackground:'yes',
      controllerHorizontalPosition:"center",
      controllerVerticalPosition:"bottom",
      inverseNextAndPrevRotation:"yes",
      addKeyboardSupport:"yes",
      slideShowAutoPlay:"no",
      startSpaceBetweenButtons:28,
      spaceBetweenButtons:20,
      startSpaceForScrollBarButtons:20,
      startSpaceForScrollBar:20,
      hideControllerDelay:300,
      controllerMaxWidth:815,
      controllerBackgroundOpacity:1,
      controllerOffsetWidth:20,
      controllerOffsetY:20,
      scrollBarOffsetX:0,
      scrollBarHandlerOffsetX:4,
      scrollBarHandlerToolTipOffsetY:4,
      zoomInAndOutToolTipOffsetY:-10,
      buttonsToolTipOffsetY:-10,
      link:"http://www.google.com",
      buttonToolTipBackgroundColor:"#FFFFFF",
      buttonToolTipFontColor:"#3A334B",
      // Navigator.
      showNavigator:"yes",
      navigatorPosition:"topRight",
      navigatorWidth:100,
      navigatorOffsetX:10,
      navigatorOffsetY:10,
      navigatorBackgroundColor:"#FFFFFF",
      navigatorHandlerColor:"#bfbf06",
      navigatorBorderColor:"#E5E4E8",
      // Info window.
      infoWindowWidth:920,
      infoWindowBackgroundOpacity:.7,
      infoWindowBackgroundColor:"#FFFFFF",
      infoWindowScrollBarColor:"#8b859b",
      // Markers.
      showMarkersInfo:"no",
      markerToolTipOffsetY:6,
      // Context menu.
      showScriptDeveloper:"yes",
      contextMenuLabels:"Rotate, Move/Pan, Rotate left, Rotate right, Zoom in/Zoom out, Play/Pause, Info, Custom link, Full screen/Normal screen",
      contextMenuBorderColor:"rgba(0,0,0,0)",
      contextMenuSpacerColor:"#E5E4E8",
      contextMenuItemTextNormalColor:"#3a334b",
      contextMenuItemTextSelectedColor:"#FFFFFF",
      contextMenuItemBackgroundNormalColor:"#FFFFFF",
      contextMenuItemBackgroundSelectedColor:"#bfbf06",
      contextMenuItemDisabledColor:"#cccccc"
    });
  }

})(jQuery, Drupal, drupalSettings);
